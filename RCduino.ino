// Include pins definition file for specific board
#include "RC_freaduino.h"

#include "RCArduinoFastLib.h"
#include "PinChangeInt.h"
#include <EEPROM.h>

// Programming and runtime modes
#define MODE_RUN 0
#define MODE_PROGRAM 1

// Interrupts flags
#define FLAG_THROTTLE 1
#define FLAG_STEERING 2

// Index into the EEPROM Storage assuming a 0 based array of uint16_t
// Data to be stored low byte, high byte
#define EEPROM_INDEX_STEERING_MIN 0
#define EEPROM_INDEX_STEERING_MAX 1
#define EEPROM_INDEX_STEERING_CENTER 2
#define EEPROM_INDEX_THROTTLE_MIN 3
#define EEPROM_INDEX_THROTTLE_MAX 4
#define EEPROM_INDEX_THROTTLE_CENTER 5

// Default channel values
#define RC_NEUTRAL 1500
#define RC_MAX 2000
#define RC_MIN 1000
#define RC_DEADBAND 40

// PWM ports output limits
#define PWM_MIN 0
#define PWM_MAX 255

// Movement type definition
#define GEAR_NONE 1
#define GEAR_IDLE 1
#define GEAR_FULL 2

#define IDLE_MAX 80

// Direction definitions
#define DIRECTION_STOP 0
#define DIRECTION_FORWARD 1
#define DIRECTION_REVERSE 2
#define DIRECTION_ROTATE_RIGHT 3
#define DIRECTION_ROTATE_LEFT 4

#define CALIBRATION_TIME 10 // seconds

uint16_t unSteeringMin = RC_MIN;
uint16_t unSteeringMax = RC_MAX;
uint16_t unSteeringCenter = RC_NEUTRAL;

uint16_t unThrottleMin = RC_MIN;
uint16_t unThrottleMax = RC_MAX;
uint16_t unThrottleCenter = RC_NEUTRAL;

uint8_t gThrottle = 0;
uint8_t gGear = GEAR_NONE;
uint8_t gOldGear = GEAR_NONE;

uint8_t gThrottleDirection = DIRECTION_STOP;
uint8_t gDirection = DIRECTION_STOP;
uint8_t gOldDirection = DIRECTION_STOP;

// Runtime mode register
uint8_t gMode = MODE_RUN;

// Calibration button state register
uint8_t unCButtonLastState = 0;
// Calibration timer
uint32_t ulProgramModeExitTime = 0;

// Register for state changes bitmask
volatile uint8_t bUpdateFlagsShared;
volatile uint16_t unThrottleInShared;
volatile uint16_t unSteeringInShared;

uint32_t ulThrottleInStart;
uint32_t ulSteeringInStart;

void setup() {
  pinMode(PIN_IN_CALIBRATION, INPUT);
  pinMode(PIN_IN_THROTTLE, INPUT);
  pinMode(PIN_IN_STEERING, INPUT);
  pinMode(PIN_OUT_LED, OUTPUT);
  
  pinMode(PIN_OUT_PWM_SPEED_LEFT, OUTPUT);
  pinMode(PIN_OUT_CONTROL_LEFT1, OUTPUT);
  pinMode(PIN_OUT_CONTROL_LEFT2, OUTPUT);

  pinMode(PIN_OUT_PWM_SPEED_RIGHT, OUTPUT);
  pinMode(PIN_OUT_CONTROL_RIGHT1, OUTPUT);
  pinMode(PIN_OUT_CONTROL_RIGHT2, OUTPUT);
  
  digitalWrite(PIN_OUT_LED, LOW);
    
  digitalWrite(PIN_OUT_CONTROL_LEFT1, LOW);
  digitalWrite(PIN_OUT_CONTROL_LEFT2, LOW);
  
  digitalWrite(PIN_OUT_CONTROL_RIGHT1, LOW);
  digitalWrite(PIN_OUT_CONTROL_RIGHT2, LOW);
  
  analogWrite(PIN_OUT_PWM_SPEED_LEFT, 0);
  analogWrite(PIN_OUT_PWM_SPEED_RIGHT, 0);
  
  CRCArduinoFastServos::begin();
  PCintPort::attachInterrupt(PIN_IN_THROTTLE, calcThrottle, CHANGE);
  PCintPort::attachInterrupt(PIN_IN_STEERING, calcSteering, CHANGE);
  
  readSettingsFromEEPROM();
}

void loop() {
  static uint16_t unThrottleIn = 0;
  static uint16_t unSteeringIn = 0;
  // local copy of update flags
  static uint8_t bUpdateFlags;
  
  checkCalibration();
  
  if(bUpdateFlagsShared) {
    noInterrupts();
    
    bUpdateFlags = bUpdateFlagsShared;
  
    if(bUpdateFlags & FLAG_THROTTLE) {
      unThrottleIn = unThrottleInShared;
    }

    if(bUpdateFlags & FLAG_STEERING) {
      unSteeringIn = unSteeringInShared;
    }
    
    bUpdateFlagsShared = 0;
    
    interrupts();
  }

  if(gMode == MODE_PROGRAM) {
    if(ulProgramModeExitTime < millis()) {
      // set to 0 to exit program mode
      ulProgramModeExitTime = 0;
      gMode = MODE_RUN;
      digitalWrite(PIN_OUT_LED, LOW);
      writeSettingsToEEPROM();
    } else {      
      if(unThrottleIn > unThrottleMax && unThrottleIn <= RC_MAX) {
        unThrottleMax = unThrottleIn;
      } else if(unThrottleIn < unThrottleMin && unThrottleIn >= RC_MIN) {
        unThrottleMin = unThrottleIn;
      }
     
      if(unSteeringIn > unSteeringMax && unSteeringIn <= RC_MAX) {
        unSteeringMax = unSteeringIn;
      } else if(unSteeringIn < unSteeringMin && unSteeringIn >= RC_MIN) {
        unSteeringMin = unSteeringIn;
      }
    }
  } else if(gMode == MODE_RUN) {
/** THROTTLE PROCESSING START **/
    // Check if there are any changes in throttle value
    if(bUpdateFlags & FLAG_THROTTLE) {
      // Limit throttle signal value to MIN/MAX possible
      unThrottleIn = constrain(unThrottleIn, unThrottleMin, unThrottleMax);
      
      // If set direction of movement
      if(unThrottleIn > unThrottleCenter) {
        gThrottle = map(unThrottleIn, unThrottleCenter, unThrottleMax, PWM_MIN, PWM_MAX);
        gThrottleDirection = DIRECTION_FORWARD;
      } else {
        gThrottle = map(unThrottleIn, unThrottleMin, unThrottleCenter, PWM_MAX, PWM_MIN);
        gThrottleDirection = DIRECTION_REVERSE;
      }
      
      // If throttle is greater then idle threshold (±80) set state to movement
      if(gThrottle < IDLE_MAX) {
        gGear = GEAR_IDLE;
      } else {
        gGear = GEAR_FULL;
      }
    }
/** THROTTLE PROCESSING END **/

/** STEERING PROCESSING START **/
  // Check if there are any changes in throttle value
    if(bUpdateFlags & FLAG_STEERING) {
      // Set starting values for both tracks to current throttle value
      uint8_t throttleLeft = gThrottle;
      uint8_t throttleRight = gThrottle;
      
      // Set direction (global) to previously calculated
      gDirection = gThrottleDirection;
      
      // Limit steering signal value to MIN/MAX possible
      unSteeringIn = constrain(unSteeringIn, unSteeringMin, unSteeringMax);
      
      // Check movement state and calculate steering for it
      switch(gGear) {
        // Throttle is in idle threshold
        // We set both tracks speed in the opposite directions
        case GEAR_IDLE:
          // Steering value is beyond dRC deadzone to the right
          if(unSteeringIn > (unSteeringCenter + RC_DEADBAND)) {
            gDirection = DIRECTION_ROTATE_RIGHT;
            // Set throttle PWM value
            // Note: the value is the same for both axis, it's the direction that is opposite 
            // But direction is set later, based on gDirection variable
            throttleRight = throttleLeft = map(unSteeringIn, unSteeringCenter, unSteeringMax, PWM_MIN, PWM_MAX);

          } else if(unSteeringIn < (unSteeringCenter - RC_DEADBAND)) {
            gDirection = DIRECTION_ROTATE_LEFT;
            // Set throttle PWM value
            // Note: the value is the same for both axis, it's the direction that is opposite 
            // But direction is set later, based on gDirection variable
            throttleRight = throttleLeft = map(unSteeringIn, unSteeringMin, unSteeringCenter, PWM_MAX, PWM_MIN);
          }
        break;
        
        // Throttle is beyopnd idle threshold = movement
        // In this case we restrain the speed of one of the tracks 
        case GEAR_FULL:
          if(unSteeringIn > (unSteeringCenter + RC_DEADBAND)) {
            throttleRight = map(unSteeringIn, unSteeringCenter, unSteeringMax, gThrottle, PWM_MIN);
          } else if(unSteeringIn < (unSteeringCenter - RC_DEADBAND)) {
            throttleLeft = map(unSteeringIn, unSteeringMin, unSteeringCenter, PWM_MIN, gThrottle);
          }
        break;
      }
      
      // Finally, set the PWM output and go, go, GO!
      analogWrite(PIN_OUT_PWM_SPEED_LEFT, throttleLeft);
      analogWrite(PIN_OUT_PWM_SPEED_RIGHT, throttleRight);
    }
/** STEERING PROCESSING END **/   
  }
  
    // Set direction PWM values  
    if((gDirection != gOldDirection) || (gGear != gOldGear)) {
      gOldDirection = gDirection;
      gOldGear = gGear;

      // First reset the movement direction to full stop
      digitalWrite(PIN_OUT_CONTROL_RIGHT1, LOW);
      digitalWrite(PIN_OUT_CONTROL_RIGHT2, LOW);
      digitalWrite(PIN_OUT_CONTROL_LEFT1, LOW);
      digitalWrite(PIN_OUT_CONTROL_LEFT2, LOW);
      
      switch(gDirection) {
        case DIRECTION_FORWARD:
          digitalWrite(PIN_OUT_CONTROL_RIGHT1, HIGH);
          digitalWrite(PIN_OUT_CONTROL_RIGHT2, LOW);
          digitalWrite(PIN_OUT_CONTROL_LEFT1, HIGH);
          digitalWrite(PIN_OUT_CONTROL_LEFT2, LOW);
        break;
        
        case DIRECTION_REVERSE:
          digitalWrite(PIN_OUT_CONTROL_RIGHT1, LOW);
          digitalWrite(PIN_OUT_CONTROL_RIGHT2, HIGH);
          digitalWrite(PIN_OUT_CONTROL_LEFT1, LOW);
          digitalWrite(PIN_OUT_CONTROL_LEFT2, HIGH);
        break;
        
        case DIRECTION_ROTATE_RIGHT:
          digitalWrite(PIN_OUT_CONTROL_RIGHT1, LOW);
          digitalWrite(PIN_OUT_CONTROL_RIGHT2, HIGH);
          digitalWrite(PIN_OUT_CONTROL_LEFT1, HIGH);
          digitalWrite(PIN_OUT_CONTROL_LEFT2, LOW);
        break;
        
        case DIRECTION_ROTATE_LEFT:
          digitalWrite(PIN_OUT_CONTROL_RIGHT1, HIGH);
          digitalWrite(PIN_OUT_CONTROL_RIGHT2, LOW);
          digitalWrite(PIN_OUT_CONTROL_LEFT1, LOW);
          digitalWrite(PIN_OUT_CONTROL_LEFT2, HIGH);
        break;
        
        case DIRECTION_STOP:
          digitalWrite(PIN_OUT_CONTROL_RIGHT1, LOW);
          digitalWrite(PIN_OUT_CONTROL_RIGHT2, LOW);
          digitalWrite(PIN_OUT_CONTROL_LEFT1, LOW);
          digitalWrite(PIN_OUT_CONTROL_LEFT2, LOW);
        break;
      }
    }
    
    bUpdateFlags = 0;
}

void calcThrottle() {
  if(PCintPort::pinState) {
    ulThrottleInStart = TCNT1;
  } else {
    unThrottleInShared = (TCNT1 - ulThrottleInStart)>>1;
    if(unThrottleInShared < RC_MAX) {
      bUpdateFlagsShared |= FLAG_THROTTLE;
    }
  }
}

void calcSteering() {
  if(PCintPort::pinState) {
    ulSteeringInStart = TCNT1;
  } else {
    unSteeringInShared = (TCNT1 - ulSteeringInStart)>>1;
    if(unSteeringInShared < RC_MAX) {
      bUpdateFlagsShared |= FLAG_STEERING;
    }
  }
}


void readSettingsFromEEPROM() {
  unSteeringMin = readChannelSetting(EEPROM_INDEX_STEERING_MIN);
  if(unSteeringMin < RC_MIN || unSteeringMin > RC_NEUTRAL) {
    unSteeringMin = RC_MIN;
  }

  unSteeringMax = readChannelSetting(EEPROM_INDEX_STEERING_MAX);
  if(unSteeringMax > RC_MAX || unSteeringMax < RC_NEUTRAL) {
    unSteeringMax = RC_MAX;
  }
  
  unSteeringCenter = readChannelSetting(EEPROM_INDEX_STEERING_CENTER);
  if(unSteeringCenter < unSteeringMin || unSteeringCenter > unSteeringMax) {
    unSteeringCenter = RC_NEUTRAL;
  }

  unThrottleMin = readChannelSetting(EEPROM_INDEX_THROTTLE_MIN);
  if(unThrottleMin < RC_MIN || unThrottleMin > RC_NEUTRAL) {
    unThrottleMin = RC_MIN;
  }

  unThrottleMax = readChannelSetting(EEPROM_INDEX_THROTTLE_MAX);
  if(unThrottleMax > RC_MAX || unThrottleMax < RC_NEUTRAL) {
    unThrottleMax = RC_MAX;
  }
  
  unThrottleCenter = readChannelSetting(EEPROM_INDEX_THROTTLE_CENTER);
  if(unThrottleCenter < unThrottleMin || unThrottleCenter > unThrottleMax) {
    unThrottleCenter = RC_NEUTRAL;
  }
}



void checkCalibration() {
  uint8_t unCButtonState = digitalRead(PIN_IN_CALIBRATION);
  
  if( unCButtonLastState != unCButtonState ) {
    if( unCButtonState == HIGH ) {
      gMode = MODE_PROGRAM;
      gDirection = DIRECTION_STOP;
      ulProgramModeExitTime = millis() + (CALIBRATION_TIME*1000);
      digitalWrite(PIN_OUT_LED, HIGH);
      unSteeringMin = unSteeringMax = unSteeringCenter = unThrottleMin = unThrottleMax = unThrottleCenter = RC_NEUTRAL;
      writeSettingsToEEPROM();
    }
  } 
  unCButtonLastState = unCButtonState;
}

void writeSettingsToEEPROM() {
  writeChannelSetting(EEPROM_INDEX_STEERING_MIN,unSteeringMin);
  writeChannelSetting(EEPROM_INDEX_STEERING_MAX,unSteeringMax);
  writeChannelSetting(EEPROM_INDEX_STEERING_CENTER,unSteeringCenter);
  writeChannelSetting(EEPROM_INDEX_THROTTLE_MIN,unThrottleMin);
  writeChannelSetting(EEPROM_INDEX_THROTTLE_MAX,unThrottleMax);
  writeChannelSetting(EEPROM_INDEX_THROTTLE_CENTER,unThrottleCenter);
}


uint16_t readChannelSetting(uint8_t nStart) {
  uint16_t unSetting = (EEPROM.read((nStart*sizeof(uint16_t))+1)<<8);
  unSetting += EEPROM.read(nStart*sizeof(uint16_t));

  return unSetting;
}

void writeChannelSetting(uint8_t nIndex,uint16_t unSetting) {
  EEPROM.write(nIndex*sizeof(uint16_t),lowByte(unSetting));
  EEPROM.write((nIndex*sizeof(uint16_t))+1,highByte(unSetting));
}
